import React, { Component } from "react";
import { ThemeProvider } from "styled-components";
import { Button } from "./Components/Button";
import { Container } from "./Components/Container";
import { Dropdown } from "./Components/Dropdown";
import {
  Heading1,
  Heading2,
  Heading3,
  Heading4,
  Heading5,
} from "./Components/Heading";
import { Table, Thead, Tbody, Tr, Th, Td } from "./Components/Table";
import { Label, Input, TextField } from "./Components/TextField";
import { ToDoListDarkTheme } from "./Theme/ToDoListDarkTheme";
import { ToDoListLightTheme } from "./Theme/ToDoListLightTheme";
import { ToDoListPrimaryTheme } from "./Theme/ToDoListPrimaryTheme";
import { connect } from "react-redux";
import { addTaskAction,doneTaskAction,deleteTaskAction, editTaskAction, updateTaskAction } from "./redux/actions/ToDoListActions";
import { arrTheme } from "../ToDoList/Theme/ThemeManager";

class ToDoList extends Component {
  state = {
    taskName: '',
  };
  renderTaskToDo = () => {
    return this.props.taskList
      .filter((task) => !task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button onClick={()=>{
                this.props.dispatch(editTaskAction(task))
              }} className="ml-1">
                <i className="fa fa-edit"></i>
              </Button>


              <Button onClick={()=>{
                this.props.dispatch(doneTaskAction(task.id))
              }} className="ml-1">
                <i className="fa fa-check"></i>
              </Button>



              <Button onClick={()=>{
                this.props.dispatch(deleteTaskAction(task.id))
              }} className="ml-1">
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  renderTaskCompleted = () => {
    return this.props.taskList
      .filter((task) => task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button onClick={()=>{
                this.props.dispatch(deleteTaskAction(task.id))
              }} className="ml-1">
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  renderTheme = () => {
    return arrTheme.map((theme, index) => {
      return <option value={theme.id}>{theme.name}</option>;
    });
  };

// static getDerivedStateFromProps(newProps,currentState){
//     let newState ={...currentState,taskName:newProps.taskEdit.taskName};
//     return newState;

//     return null;
// }
  render() {
    return (
      <div>
        <ThemeProvider theme={this.props.themToDoList}>
          <Container className="w-50">
            <Dropdown onChange={(e)=>{

                let{value} = e.target;
                this.props.dispatch({
                    type:'change_theme',
                    themeId : value
                })

            }}>{this.renderTheme()}</Dropdown>
            <Heading3>To Do List</Heading3>
            <TextField value={this.state.taskName}
              onChange={(e)=>{
                this.setState({
                    taskName:e.target.value
                },()=>{

                    console.log(this.state);
                })
              }}
              label="Task name"
              className="w-50"
            ></TextField>
            <Button
              onClick={() => {
                let { taskName } = this.state;
                let newTask = {
                  id: Date.now(),
                  taskName: taskName,
                  done: false,
                };

                this.props.dispatch(addTaskAction(newTask));
              }}
              className="ml-2"
            >
              <i className="fa fa-plus"></i> Add Task
            </Button>
            <Button onClick={()=>{
                this.props.dispatch(updateTaskAction(this.state.taskName))
            }} className="ml-2">
              <i className="fa fa-upload"></i> Update Task
            </Button>
            <hr></hr>
            <Heading3>Task To Do</Heading3>
            <Table>
              <Thead>{this.renderTaskToDo()}</Thead>
            </Table>
            <Heading3>Task Completed</Heading3>
            <Table>
              <Thead>{this.renderTaskCompleted()}</Thead>
            </Table>
          </Container>
        </ThemeProvider>
      </div>
    );
  }
  componentDidUpdate(prevProps,prevState){
    if(prevProps.taskEdit.id !== this.props.taskEdit.id){
        this.setState({
            taskName:this.props.taskEdit.taskName,
        })
    }
  }
}

const mapStateToProps = (state) => {
  return {
    themToDoList: state.ToDoListReducer.themeToDoList,
    taskList: state.ToDoListReducer.taskList,
    taskEdit:state.ToDoListReducer.taskEdit
  };
};




export default connect(mapStateToProps)(ToDoList);
